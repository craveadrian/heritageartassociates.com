<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Heritage_Art_Associates
 * @since 1.0.0
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info container">
			<div class="site-footer__nav col-xl-12">
				<?php get_template_part( 'template-parts/navigation/navigation', 'bottom' ); ?>
			</div>
			<div class="site-footer__info col-xl-12">
				<?php get_template_part( 'template-parts/footer/site', 'info' ); ?>
				
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
