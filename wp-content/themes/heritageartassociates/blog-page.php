<?php
/**
 * Template Name: Blog with Sidebar
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Heritage_Art_Associates
 * @since 1.0.0
 */

get_header();
?>

	<section id="primary" class="content-area container with-sidebar">
		<main id="main" class="site-main">

			<?php

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}

			endwhile; // End of the loop.
			?>
			<div class="blog-single"> 

				<div class="blog-text" id="blg-post-<?php the_ID(); ?>">

					<div class="left">

						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail',  $attr); ?></a>

					</div>

					<div class="right">

						<h3 class="blog-title">

							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

						</h3>

						<div class="blog-excerpt">

							<?php the_excerpt(); ?>

						</div>

						<a href="<?php the_permalink(); ?>" class="bg-link btn">Read More</a>

					</div>

				</div>

			</div> 

		</main>
	</section>

<?php
get_footer();
