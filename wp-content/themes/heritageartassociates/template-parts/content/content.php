<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Heritage_Art_Associates
 * @since 1.0.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="entry-header__date">
			<time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished">
				<span class="month"><?php echo get_the_date(__('M')); ?></span>
				<span class="day"><?php echo get_the_date(__('d')); ?></span>
				<span class="year"><?php echo get_the_date(__('Y')); ?></span>
			</time>
		</div>
		<div class="entry-header__text">
			<?php
			if (is_sticky() && is_home() && !is_paged()) {
				printf('<span class="sticky-post">%s</span>', _x('Featured', 'post', 'heritageartassociates'));
			}
			if (is_singular()) :
				the_title('<h1 class="entry-title">', '</h1>');
			else :
				the_title(sprintf('<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>');
			endif;
			?>
			<p>Author: <?php echo get_the_author(); ?></p>
		</div>
		
	</header><!-- .entry-header -->

	<?php heritageartassociates_post_thumbnail(); ?>

	<div class="entry-content">
		<?php if (is_single()) :
			the_content(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__('Continue reading<span class="screen-reader-text"> "%s"</span>', 'heritageartassociates'),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . __('Pages:', 'heritageartassociates'),
					'after'  => '</div>',
				)
			);
		else :
			the_excerpt();
			?>
			<div class="btn btn-primary">
				<a href="<?php echo get_permalink() ?>" >READ MORE</a>
			</div>
		<?php endif; ?>
	</div><!-- .entry-content -->

	<!-- <footer class="entry-footer">
		<?php //heritageartassociates_entry_footer(); ?>
	</footer> -->
	<!-- .entry-footer -->
</article><!-- #post-${ID} -->