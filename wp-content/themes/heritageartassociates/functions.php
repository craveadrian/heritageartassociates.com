<?php
/**
 * Heritage Art Associates functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Heritage_Art_Associates
 * @since 1.0.0
 */

/**
 * Heritage Art Associates only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

if ( ! function_exists( 'heritageartassociates_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function heritageartassociates_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Heritage Art Associates, use a find and replace
		 * to change 'heritageartassociates' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'heritageartassociates', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1568, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'top' => __( 'Primary', 'heritageartassociates' ),
				'footer' => __( 'Footer Menu', 'heritageartassociates' ),
				'social' => __( 'Social Links Menu', 'heritageartassociates' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 300,
				'width'       => 300,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for Block Styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for editor styles.
		add_theme_support( 'editor-styles' );

		// Enqueue editor styles.
		add_editor_style( 'style-editor.css' );

		// Add custom editor font sizes.
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => __( 'Small', 'heritageartassociates' ),
					'shortName' => __( 'S', 'heritageartassociates' ),
					'size'      => 19.5,
					'slug'      => 'small',
				),
				array(
					'name'      => __( 'Normal', 'heritageartassociates' ),
					'shortName' => __( 'M', 'heritageartassociates' ),
					'size'      => 22,
					'slug'      => 'normal',
				),
				array(
					'name'      => __( 'Large', 'heritageartassociates' ),
					'shortName' => __( 'L', 'heritageartassociates' ),
					'size'      => 36.5,
					'slug'      => 'large',
				),
				array(
					'name'      => __( 'Huge', 'heritageartassociates' ),
					'shortName' => __( 'XL', 'heritageartassociates' ),
					'size'      => 49.5,
					'slug'      => 'huge',
				),
			)
		);

		// Editor color palette.
		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Primary', 'heritageartassociates' ),
					'slug'  => 'primary',
					'color' => heritageartassociates_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 33 ),
				),
				array(
					'name'  => __( 'Secondary', 'heritageartassociates' ),
					'slug'  => 'secondary',
					'color' => heritageartassociates_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 23 ),
				),
				array(
					'name'  => __( 'Dark Gray', 'heritageartassociates' ),
					'slug'  => 'dark-gray',
					'color' => '#111',
				),
				array(
					'name'  => __( 'Light Gray', 'heritageartassociates' ),
					'slug'  => 'light-gray',
					'color' => '#767676',
				),
				array(
					'name'  => __( 'White', 'heritageartassociates' ),
					'slug'  => 'white',
					'color' => '#FFF',
				),
			)
		);

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );
	}
endif;
add_action( 'after_setup_theme', 'heritageartassociates_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function heritageartassociates_widgets_init() {

	register_sidebar(
		array(
			'name'          => __( 'Site Banner Area', 'heritageartassociates' ),
			'id'            => 'site-banner',
			'description'   => __( 'Primarily a location for banner and slideshow widgets.', 'heritageartassociates' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="title">',
			'after_title'   => '</h1>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Blog Sidebar', 'heritageartassociates' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your blog sidebar.', 'heritageartassociates' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

}
add_action( 'widgets_init', 'heritageartassociates_widgets_init' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width Content width.
 */
function heritageartassociates_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'heritageartassociates_content_width', 640 );
}
add_action( 'after_setup_theme', 'heritageartassociates_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function heritageartassociates_scripts() {

	wp_enqueue_style( 'heritageartassociates-style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );

	wp_style_add_data( 'heritageartassociates-style', 'rtl', 'replace' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Navigation
	//wp_enqueue_style( 'heritageartassociates-theme-base', get_template_directory_uri() . '/assets/css/navigation.css');

	// Social Menu
	wp_enqueue_style( 'heritageartassociates-social-menu', get_template_directory_uri() . '/assets/css/social-menu.css');

	// Grid
	wp_enqueue_style( 'heritageartassociates-grids', get_template_directory_uri() . '/grid.css');

	//Scroll to Content
	wp_enqueue_script( 'heritageartassociates-scripts', get_template_directory_uri() . '/assets/js/scripts.js' );

	// Font Awesome
	wp_enqueue_style( 'heritageartassociates-font-awesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
}
add_action( 'wp_enqueue_scripts', 'heritageartassociates_scripts' );

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function heritageartassociates_skip_link_focus_fix() {
	// The following is minified via `terser --compress --mangle -- js/skip-link-focus-fix.js`.
	?>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
	<?php
}
add_action( 'wp_print_footer_scripts', 'heritageartassociates_skip_link_focus_fix' );

/**
 * Enqueue supplemental block editor styles.
 */
function heritageartassociates_editor_customizer_styles() {

	wp_enqueue_style( 'heritageartassociates-editor-customizer-styles', get_theme_file_uri( '/style-editor-customizer.css' ), false, '1.0', 'all' );

	if ( 'custom' === get_theme_mod( 'primary_color' ) ) {
		// Include color patterns.
		require_once get_parent_theme_file_path( '/inc/color-patterns.php' );
		wp_add_inline_style( 'heritageartassociates-editor-customizer-styles', heritageartassociates_custom_colors_css() );
	}
}
add_action( 'enqueue_block_editor_assets', 'heritageartassociates_editor_customizer_styles' );

/**
 * Display custom color CSS in customizer and on frontend.
 */
function heritageartassociates_colors_css_wrap() {

	// Only include custom colors in customizer or frontend.
	if ( ( ! is_customize_preview() && 'default' === get_theme_mod( 'primary_color', 'default' ) ) || is_admin() ) {
		return;
	}

	require_once get_parent_theme_file_path( '/inc/color-patterns.php' );

	$primary_color = 199;
	if ( 'default' !== get_theme_mod( 'primary_color', 'default' ) ) {
		$primary_color = get_theme_mod( 'primary_color_hue', 199 );
	}
	?>

	<style type="text/css" id="custom-theme-colors" <?php echo is_customize_preview() ? 'data-hue="' . absint( $primary_color ) . '"' : ''; ?>>
		<?php echo heritageartassociates_custom_colors_css(); ?>
	</style>
	<?php
}
add_action( 'wp_head', 'heritageartassociates_colors_css_wrap' );

/**
 * SVG Icons class.
 */
require get_template_directory() . '/classes/class-heritageartassociates-svg-icons.php';

/**
 * Custom Comment Walker template.
 */
require get_template_directory() . '/classes/class-heritageartassociates-walker-comment.php';

/**
 * Enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * SVG Icons related functions.
 */
require get_template_directory() . '/inc/icon-functions.php';

/**
 * Custom template tags for the theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Theme options shortcode generator.
 */
require get_template_directory() . '/inc/shortcode-generator.php';

/**
 * Custom Post Types.
 */
//require get_template_directory() . '/inc/custom-post-types.php';


function wp5_hook_fonts() {
    ?>
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
		<script>
		WebFont.load({
			google: {
				families: ['Open Sans:400,700']
			}
		});
		</script>
	<?php
}
add_action('wp_footer', 'wp5_hook_fonts');


add_filter( 'body_class', 'inner_body_class' );

function inner_body_class( $classes ) {
	global $post;
	$pagename = $post->post_name;
	if ( ! is_front_page() ) {
		$classes[] = 'inner ' . $pagename;
	}
	return $classes;
}

//Fixed Script & CSS type warning in validation.
add_action( 'template_redirect', function(){
    ob_start( function( $buffer ){
        $buffer = str_replace( array( 'type="text/javascript"', "type='text/javascript'", 'type="text/css"', "type='text/css'" ), '', $buffer );

        return $buffer;
    });
});

add_filter('wpcf7_autop_or_not', '__return_false');