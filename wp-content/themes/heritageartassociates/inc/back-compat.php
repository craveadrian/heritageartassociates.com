<?php
/**
 * Heritage Art Associates back compat functionality
 *
 * Prevents Heritage Art Associates from running on WordPress versions prior to 4.7,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.7.
 *
 * @package WordPress
 * @subpackage Heritage_Art_Associates
 * @since Heritage Art Associates 1.0.0
 */

/**
 * Prevent switching to Heritage Art Associates on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since Heritage Art Associates 1.0.0
 */
function heritageartassociates_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'heritageartassociates_upgrade_notice' );
}
add_action( 'after_switch_theme', 'heritageartassociates_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Heritage Art Associates on WordPress versions prior to 4.7.
 *
 * @since Heritage Art Associates 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function heritageartassociates_upgrade_notice() {
	$message = sprintf( __( 'Heritage Art Associates requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'heritageartassociates' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.7.
 *
 * @since Heritage Art Associates 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function heritageartassociates_customize() {
	wp_die(
		sprintf(
			__( 'Heritage Art Associates requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'heritageartassociates' ),
			$GLOBALS['wp_version']
		),
		'',
		array(
			'back_link' => true,
		)
	);
}
add_action( 'load-customize.php', 'heritageartassociates_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.7.
 *
 * @since Heritage Art Associates 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function heritageartassociates_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'Heritage Art Associates requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'heritageartassociates' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'heritageartassociates_preview' );
