<?php
/**
 * WP Default Custom Post Types Sample
 *
 * @package WP_Default
 */

/**
 * Create fence post type
 */
function fence_post_type() {

	$labels = array(
		'name'					=> __( 'Fences', 'heritageartassociates' ),
		'singular_name'			=> __( 'Fence', 'heritageartassociates' ),
		'add_new'				=> __( 'New Fence', 'heritageartassociates' ),
		'add_new_item'			=> __( 'Add New Fence', 'heritageartassociates' ),
		'edit_item'				=> __( 'Edit Fence', 'heritageartassociates' ),
		'new_item'				=> __( 'New Fence', 'heritageartassociates' ),
		'view_item'				=> __( 'View Fence', 'heritageartassociates' ),
		'search_items'			=> __( 'Search Fences', 'heritageartassociates' ),
		'not_found'				=>  __( 'No Fences Found', 'heritageartassociates' ),
		'not_found_in_trash'	=> __( 'No Fences found in Trash', 'heritageartassociates' ),
	);
	$args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'menu_icon'		=> 'dashicons-portfolio',
		'rewrite'		=> array( 'slug' => 'fence' ),
		'supports'		=> array(
			'title',
			'editor',
			'excerpt',
			'custom-fields',
			'thumbnail',
			'page-attributes'
		),
		'taxonomies'	=> array( 'post_tag' ),
	);
	register_post_type( 'fence', $args );

}
add_action( 'init', 'fence_post_type' );

/**
 * Create fence post type taxonomy
 */
function register_fence_taxonomy() {

	$labels = array(
		'name'				=> __( 'Categories', 'heritageartassociates' ),
		'singular_name'		=> __( 'Category', 'heritageartassociates' ),
		'search_items'		=> __( 'Search Categories', 'heritageartassociates' ),
		'all_items'			=> __( 'All Categories', 'heritageartassociates' ),
		'edit_item'			=> __( 'Edit Category', 'heritageartassociates' ),
		'update_item'		=> __( 'Update Category', 'heritageartassociates' ),
		'add_new_item'		=> __( 'Add New Category', 'heritageartassociates' ),
		'new_item_name'		=> __( 'New Category Name', 'heritageartassociates' ),
		'menu_name'			=> __( 'Categories', 'heritageartassociates' ),
	);

	$args = array(
		'labels'			=> $labels,
		'hierarchical'		=> true,
		'sort'				=> true,
		'args'				=> array( 'orderby' => 'term_order' ),
		'rewrite'			=> array( 'slug'    => 'fence' ),
		'show_admin_column'	=> true
	);

	register_taxonomy( 'fence_cat', array( 'fence' ), $args);

}
add_action( 'init', 'register_fence_taxonomy' );