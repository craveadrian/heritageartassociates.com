<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'heritageartassociates' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=^iThmDSWI49TDrIq$*&d>nyD{.5J+T-vfA3EZ1A4p!Ws!E?+hFpwn-sl2F]mG Y' );
define( 'SECURE_AUTH_KEY',  '1^$gmVWT##>`|Wa <q];/ [pXUO 1ID!{wzNnm>KDh|^TbIHH}u|yYZhY4r5EmHe' );
define( 'LOGGED_IN_KEY',    '>D23Yn6-zNZR[T_!38v bUN?_/wATqTmZ8V~.f7 NYZ/SXbJ`M*u@bC*^Wp=ac@g' );
define( 'NONCE_KEY',        'FcA7{f}i]Q/0{|Y^PK/,?~9(rcDs&J.?q.x4cit4-pQ5Paf&ochMwq[}E>4EgLz<' );
define( 'AUTH_SALT',        'BWz$dMWM[{d4h%FI,lo,y}%}V=H33=8F`^hQvychgl&?klSHo(}.he1aqc{++n2E' );
define( 'SECURE_AUTH_SALT', 'rNS]|@C*7e LgG]1<QN:q!tn.3#<n>WZr,-utM:[96!(y<R$dH=`r_fp?3ea9oQ-' );
define( 'LOGGED_IN_SALT',   ';Qu7F*Lw7KOdcEPHs*mnSIKvty(]o0<O1Ww-7YaU0I8szQa?sybNM!:iX^11~ Wq' );
define( 'NONCE_SALT',       'vMxGaqH?97vRWbcVvc2`Y#~jD3SxyK&oFxMZ0jrHhdiQ*0;#b~sTp-{|e=/$oww!' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
